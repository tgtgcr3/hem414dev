import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getServicesList = createAsyncThunk('ServicesList/todosFetched', async() => {
    const response = collection(db, 'ServicesList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ServicesListSlice = createSlice({
    name: 'ServicesList',
    initialState: {
        allServicesList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getServicesList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getServicesList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allServicesList = action.payload
        },
        [getServicesList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ServicesListReducer = ServicesListSlice.reducer

// Selector
export const ServicesListSelector = state => state.ServicesListReducer.allServicesList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ServicesListSlice.actions

// Export reducer
export default ServicesListReducer