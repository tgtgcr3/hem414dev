import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs, query, where } from "firebase/firestore";
import db from '../../config/firebase'
import Buffer from "buffer"

// Reducer Thunk
export const getUsers = createAsyncThunk('Users/getedUsers', async User => {
    const Passwordbtoa = btoa(User.password);
    const response = query(collection(db, 'Users'), where("UserName", "==", User.username),  where ("Password", "==", Passwordbtoa ));
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    // let stringToBase64 = btoa(docList[0].Password);
    // let stringToBase64 = atob("dGd0Z2Ny");
    // console.log(stringToBase64)
    // console.log(docList[0].Password)
    console.log(docList.username)
    return docList.username
})


const UsersSlice = createSlice({
    name: 'Users',
    initialState: {
        allUsers: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getUsers.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getUsers.fulfilled]: (state, action) => {
            console.log('Done')
            state.allUsers = action.payload
            console.log(state.allUsers)
        },
        [getUsers.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const UsersReducer = UsersSlice.reducer

// Selector
export const UsersSelector = state => state.UsersReducer.allUsers

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = UsersSlice.actions

// Export reducer
export default UsersReducer