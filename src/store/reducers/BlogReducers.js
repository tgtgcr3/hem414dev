import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getBlog = createAsyncThunk('Blog/todosFetched', async() => {
    const response = collection(db, 'Blog');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const BlogSlice = createSlice({
    name: 'Blog',
    initialState: {
        allBlog: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getBlog.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getBlog.fulfilled]: (state, action) => {
            console.log('Done')
            state.allBlog = action.payload
        },
        [getBlog.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const BlogReducer = BlogSlice.reducer

// Selector
export const BlogSelector = state => state.BlogReducer.allBlog

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = BlogSlice.actions

// Export reducer
export default BlogReducer