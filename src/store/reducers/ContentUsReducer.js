import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getContentUs = createAsyncThunk('ContentUs/todosFetched', async() => {
    const response = collection(db, 'ContentUs');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const ContentUsSlice = createSlice({
    name: 'ContentUs',
    initialState: {
        allContentUs: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getContentUs.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getContentUs.fulfilled]: (state, action) => {
            console.log('Done')
            state.allContentUs = action.payload
        },
        [getContentUs.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ContentUsReducer = ContentUsSlice.reducer

// Selector
export const ContentUsSelector = state => state.ContentUsReducer.allContentUs

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ContentUsSlice.actions

// Export reducer
export default ContentUsReducer