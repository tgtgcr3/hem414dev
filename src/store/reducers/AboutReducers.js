import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getAbout = createAsyncThunk('About/todosFetched', async() => {
    const response = collection(db, 'About');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const AboutSlice = createSlice({
    name: 'About',
    initialState: {
        allAbout: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getAbout.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getAbout.fulfilled]: (state, action) => {
            console.log('Done')
            state.allAbout = action.payload
        },
        [getAbout.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const AboutReducer = AboutSlice.reducer

// Selector
export const AboutSelector = state => state.AboutReducer.allAbout

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = AboutSlice.actions

// Export reducer
export default AboutReducer