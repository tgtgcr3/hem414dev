import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getContactInformation = createAsyncThunk('ContactInformation/todosFetched', async() => {
    const response = collection(db, 'ContactInformation');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ContactInformationSlice = createSlice({
    name: 'ContactInformation',
    initialState: {
        allContactInformation: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getContactInformation.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getContactInformation.fulfilled]: (state, action) => {
            console.log('Done')
            state.allContactInformation = action.payload
        },
        [getContactInformation.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ContactInformationReducer = ContactInformationSlice.reducer

// Selector
export const ContactInformationSelector = state => state.ContactInformationReducer.allContactInformation

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ContactInformationSlice.actions

// Export reducer
export default ContactInformationReducer