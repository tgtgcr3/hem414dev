import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getSkillItem = createAsyncThunk('SkillItem/todosFetched', async() => {
    const response = collection(db, 'SkillItem');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const SkillItemSlice = createSlice({
    name: 'SkillItem',
    initialState: {
        allSkillItem: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getSkillItem.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getSkillItem.fulfilled]: (state, action) => {
            console.log('Done')
            state.allSkillItem = action.payload
        },
        [getSkillItem.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const SkillItemReducer = SkillItemSlice.reducer

// Selector
export const SkillItemSelector = state => state.SkillItemReducer.allSkillItem

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = SkillItemSlice.actions

// Export reducer
export default SkillItemReducer