import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'
import { PublicContext } from '../../contexts/publicContext'
import { useContext } from 'react'

// Reducer Thunk
export const getOverview = createAsyncThunk('Overview/todosFetched', async () => {
    try {
        const response = collection(db, 'Overview');
        const docSnapshot = await getDocs(response);
        const docList = docSnapshot.docs.map(doc => doc.data());
        return docList
    }
    catch (error) {
        return error
    }

})

const OverviewSlice = createSlice({
    name: 'Overview',
    initialState: {
        allTodos: []
    },
    reducers: {
    },
    extraReducers: {
        //get todos by name

        // Get all todos
        [getOverview.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.loading = true
        },
        [getOverview.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTodos = action.payload
            state.loading = false

        },
        [getOverview.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            console.log(action)
            state.loading = false

        }
    }
})

// Reducer
const OverviewReducer = OverviewSlice.reducer

// Selector
export const OverviewSelector = state => state.OverviewReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = OverviewSlice.actions

// Export reducer
export default OverviewReducer