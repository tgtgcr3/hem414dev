import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getProject = createAsyncThunk('Project/todosFetched', async() => {
    const response = collection(db, 'Project');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ProjectSlice = createSlice({
    name: 'Project',
    initialState: {
        allProject: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getProject.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getProject.fulfilled]: (state, action) => {
            console.log('Done')
            state.allProject = action.payload
        },
        [getProject.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ProjectReducer = ProjectSlice.reducer

// Selector
export const ProjectSelector = state => state.ProjectReducer.allProject

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ProjectSlice.actions

// Export reducer
export default ProjectReducer