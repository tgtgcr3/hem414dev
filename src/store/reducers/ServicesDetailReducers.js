import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getServicesDetail = createAsyncThunk('ServicesDetail/todosFetched', async() => {
    const response = collection(db, 'ServicesDetail');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ServicesDetailSlice = createSlice({
    name: 'ServicesDetail',
    initialState: {
        allServicesDetail: [],
    },
    reducers: {},
    extraReducers: {

        // Get all todos
        [getServicesDetail.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getServicesDetail.fulfilled]: (state, action) => {
            console.log('Done')
            state.allServicesDetail = action.payload
        },
        [getServicesDetail.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ServicesDetailReducer = ServicesDetailSlice.reducer

// Selector
export const ServicesDetailSelector = state => state.ServicesDetailReducer.allServicesDetail

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ServicesDetailSlice.actions

// Export reducer
export default ServicesDetailReducer