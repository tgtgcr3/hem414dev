import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getCounterList = createAsyncThunk('CounterList/todosFetched', async() => {
    const response = collection(db, 'CounterList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const CounterListSlice = createSlice({
    name: 'CounterList',
    initialState: {
        allCounterList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getCounterList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getCounterList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allCounterList = action.payload
        },
        [getCounterList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const CounterListReducer = CounterListSlice.reducer

// Selector
export const CounterListSelector = state => state.CounterListReducer.allCounterList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = CounterListSlice.actions

// Export reducer
export default CounterListReducer