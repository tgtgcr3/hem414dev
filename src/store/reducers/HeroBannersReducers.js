import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getHeroBanners = createAsyncThunk('HeroBanners/todosFetched', async() => {
    const response = collection(db, 'HeroBanners');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const HeroBannersSlice = createSlice({
    name: 'HeroBanners',
    initialState: {
        allTodos: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getHeroBanners.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.loading = true
        },
        [getHeroBanners.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTodos = action.payload
            state.loading = false
        },
        [getHeroBanners.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.loading = false
        }
    }
})

// Reducer
const HeroBannersReducer = HeroBannersSlice.reducer

// Selector
export const HeroBannersSelector = state => state.HeroBannersReducer

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = HeroBannersSlice.actions

// Export reducer
export default HeroBannersReducer