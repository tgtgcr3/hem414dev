import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getTeamList = createAsyncThunk('TeamList/todosFetched', async() => {
    const response = collection(db, 'TeamList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const TeamListSlice = createSlice({
    name: 'TeamList',
    initialState: {
        allTeamList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getTeamList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getTeamList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTeamList = action.payload
        },
        [getTeamList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const TeamListReducer = TeamListSlice.reducer

// Selector
export const TeamListSelector = state => state.TeamListReducer.allTeamList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = TeamListSlice.actions

// Export reducer
export default TeamListReducer