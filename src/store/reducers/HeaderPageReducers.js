import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getHeaderPage = createAsyncThunk('HeaderPage/todosFetched', async() => {
    const response = collection(db, 'HeaderPage');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const HeaderPageSlice = createSlice({
    name: 'HeaderPage',
    initialState: {
        allTodos: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getHeaderPage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getHeaderPage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTodos = action.payload
        },
        [getHeaderPage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const HeaderPageReducer = HeaderPageSlice.reducer

// Selector
export const HeaderPageSelector = state => state.HeaderPageReducer.allTodos

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = HeaderPageSlice.actions

// Export reducer
export default HeaderPageReducer