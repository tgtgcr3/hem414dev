import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getCounter = createAsyncThunk('Counter/todosFetched', async() => {
    const response = collection(db, 'Counter');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const CounterSlice = createSlice({
    name: 'Counter',
    initialState: {
        allCounter: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getCounter.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getCounter.fulfilled]: (state, action) => {
            console.log('Done')
            state.allCounter = action.payload
        },
        [getCounter.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const CounterReducer = CounterSlice.reducer

// Selector
export const CounterSelector = state => state.CounterReducer.allCounter

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = CounterSlice.actions

// Export reducer
export default CounterReducer