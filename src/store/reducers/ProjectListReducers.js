import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getProjectList = createAsyncThunk('ProjectList/todosFetched', async() => {
    const response = collection(db, 'ProjectList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ProjectListSlice = createSlice({
    name: 'ProjectList',
    initialState: {
        allProjectList: [],
        project_name: 'all'
    },
    reducers: {
        findPost(state, action) {
            const postID = action.payload
            state.project_name = postID
        }
    },
    extraReducers: {
        //get todos by name

        // Get all todos
        [getProjectList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getProjectList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allProjectList = action.payload
        },
        [getProjectList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ProjectListReducer = ProjectListSlice.reducer

// Selector
export const ProjectListSelector = state => state.ProjectListReducer

// Action export
export const {
    findPost
    // addTodo,
    // deleteTodo
    // todosFetched
} = ProjectListSlice.actions

// Export reducer
export default ProjectListReducer