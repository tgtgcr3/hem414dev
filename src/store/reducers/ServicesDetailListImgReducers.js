import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getServicesDetailListImg = createAsyncThunk('ServicesDetailListImg/todosFetched', async() => {
    const response = collection(db, 'ServicesDetailListImg');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ServicesDetailListImgSlice = createSlice({
    name: 'ServicesDetailListImg',
    initialState: {
        allServicesDetailListImg: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getServicesDetailListImg.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getServicesDetailListImg.fulfilled]: (state, action) => {
            console.log('Done')
            state.allServicesDetailListImg = action.payload
        },
        [getServicesDetailListImg.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ServicesDetailListImgReducer = ServicesDetailListImgSlice.reducer

// Selector
export const ServicesDetailListImgSelector = state => state.ServicesDetailListImgReducer.allServicesDetailListImg

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ServicesDetailListImgSlice.actions

// Export reducer
export default ServicesDetailListImgReducer