import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getTestimonial = createAsyncThunk('Testimonial/todosFetched', async() => {
    const response = collection(db, 'Testimonial');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const TestimonialSlice = createSlice({
    name: 'Testimonial',
    initialState: {
        allTestimonial: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getTestimonial.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getTestimonial.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTestimonial = action.payload
        },
        [getTestimonial.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const TestimonialReducer = TestimonialSlice.reducer

// Selector
export const TestimonialSelector = state => state.TestimonialReducer.allTestimonial

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = TestimonialSlice.actions

// Export reducer
export default TestimonialReducer