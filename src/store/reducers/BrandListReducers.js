import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getBrandList = createAsyncThunk('BrandList/todosFetched', async() => {
    const response = collection(db, 'BrandList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const BrandListSlice = createSlice({
    name: 'BrandList',
    initialState: {
        allBrandList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getBrandList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getBrandList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allBrandList = action.payload
        },
        [getBrandList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const BrandListReducer = BrandListSlice.reducer

// Selector
export const BrandListSelector = state => state.BrandListReducer.allBrandList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = BrandListSlice.actions

// Export reducer
export default BrandListReducer