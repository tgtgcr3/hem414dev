import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getFeaturesList = createAsyncThunk('FeaturesList/todosFetched', async() => {
    const response = collection(db, 'FeaturesList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const FeaturesListSlice = createSlice({
    name: 'FeaturesList',
    initialState: {
        allFeaturesList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getFeaturesList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getFeaturesList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allFeaturesList = action.payload
        },
        [getFeaturesList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const FeaturesListReducer = FeaturesListSlice.reducer

// Selector
export const FeaturesListSelector = state => state.FeaturesListReducer.allFeaturesList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = FeaturesListSlice.actions

// Export reducer
export default FeaturesListReducer