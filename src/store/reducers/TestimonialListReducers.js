import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getTestimonialList = createAsyncThunk('TestimonialList/todosFetched', async() => {
    const response = collection(db, 'TestimonialList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const TestimonialListSlice = createSlice({
    name: 'TestimonialList',
    initialState: {
        allTestimonialList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getTestimonialList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getTestimonialList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTestimonialList = action.payload
        },
        [getTestimonialList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const TestimonialListReducer = TestimonialListSlice.reducer

// Selector
export const TestimonialListSelector = state => state.TestimonialListReducer.allTestimonialList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = TestimonialListSlice.actions

// Export reducer
export default TestimonialListReducer