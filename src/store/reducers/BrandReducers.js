import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getBrand = createAsyncThunk('Brand/todosFetched', async() => {
    const response = collection(db, 'Brand');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const BrandSlice = createSlice({
    name: 'Brand',
    initialState: {
        allBrand: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getBrand.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getBrand.fulfilled]: (state, action) => {
            console.log('Done')
            state.allBrand = action.payload
        },
        [getBrand.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const BrandReducer = BrandSlice.reducer

// Selector
export const BrandSelector = state => state.BrandReducer.allBrand

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = BrandSlice.actions

// Export reducer
export default BrandReducer