import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getBlogList = createAsyncThunk('blogList/todosFetched', async() => {
    const response = collection(db, 'blogList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const BlogListSlice = createSlice({
    name: 'blogList',
    initialState: {
        allBlogList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getBlogList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getBlogList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allBlogList = action.payload
        },
        [getBlogList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const BlogListReducer = BlogListSlice.reducer

// Selector
export const BlogListSelector = state => state.BlogListReducer.allBlogList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = BlogListSlice.actions

// Export reducer
export default BlogListReducer