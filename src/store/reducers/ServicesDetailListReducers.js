import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getServicesDetailList = createAsyncThunk('ServicesDetailList/todosFetched', async() => {
    const response = collection(db, 'ServicesDetailList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ServicesDetailListSlice = createSlice({
    name: 'ServicesDetailList',
    initialState: {
        allServicesDetailList: [],
    },
    reducers: {},
    extraReducers: {

        // Get all todos
        [getServicesDetailList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getServicesDetailList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allServicesDetailList = action.payload
        },
        [getServicesDetailList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ServicesDetailListReducer = ServicesDetailListSlice.reducer

// Selector
export const ServicesDetailListSelector = state => state.ServicesDetailListReducer.allServicesDetailList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ServicesDetailListSlice.actions

// Export reducer
export default ServicesDetailListReducer