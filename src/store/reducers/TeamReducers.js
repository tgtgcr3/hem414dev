import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getTeam = createAsyncThunk('Team/todosFetched', async() => {
    const response = collection(db, 'Team');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());

    return docList
})


const TeamSlice = createSlice({
    name: 'Team',
    initialState: {
        allTeam: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getTeam.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getTeam.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTeam = action.payload
        },
        [getTeam.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const TeamReducer = TeamSlice.reducer

// Selector
export const TeamSelector = state => state.TeamReducer.allTeam

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = TeamSlice.actions

// Export reducer
export default TeamReducer