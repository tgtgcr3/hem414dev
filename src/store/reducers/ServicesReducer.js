import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getTodos = createAsyncThunk('todos/todosFetched', async() => {
    const response = collection(db, 'Services');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const todosSlice = createSlice({
    name: 'todos',
    initialState: {
        allTodos: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getTodos.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getTodos.fulfilled]: (state, action) => {
            console.log('Done')
            state.allTodos = action.payload
        },
        [getTodos.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const todosReducer = todosSlice.reducer

// Selector
export const todosSelector = state => state.todosReducer.allTodos

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = todosSlice.actions

// Export reducer
export default todosReducer