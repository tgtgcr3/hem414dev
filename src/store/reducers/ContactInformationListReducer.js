import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getContactInformationList = createAsyncThunk('ContactInformationList/todosFetched', async() => {
    const response = collection(db, 'ContactInformationList');
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ContactInformationListSlice = createSlice({
    name: 'ContactInformationList',
    initialState: {
        allContactInformationList: [],
    },
    reducers: {},
    extraReducers: {
        //get todos by name

        // Get all todos
        [getContactInformationList.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
        },
        [getContactInformationList.fulfilled]: (state, action) => {
            console.log('Done')
            state.allContactInformationList = action.payload
        },
        [getContactInformationList.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
        }
    }
})

// Reducer
const ContactInformationListReducer = ContactInformationListSlice.reducer

// Selector
export const ContactInformationListSelector = state => state.ContactInformationListReducer.allContactInformationList

// Action export
export const {
    // addTodo,
    // deleteTodo
    // todosFetched
} = ContactInformationListSlice.actions

// Export reducer
export default ContactInformationListReducer