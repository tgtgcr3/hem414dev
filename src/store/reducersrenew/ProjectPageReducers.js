import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import { collection, getDocs } from "firebase/firestore";
import db from '../../config/firebase'



// Reducer Thunk
export const getProjectPage = createAsyncThunk('ProjectPage/todosFetched', async() => {
    const response = collection(db, "ProjectPage");
    const docSnapshot = await getDocs(response);
    const docList = docSnapshot.docs.map(doc => doc.data());
    return docList
})


const ProjectPageSlice = createSlice({
    name: 'ProjectPage',
    initialState: {
        allPageLoad: [],
        isLoading: true,
        project_name: 'all'
    },
    reducers: {
        findPost(state, action) {
            const postID = action.payload
            state.project_name = postID
        }
    },
    extraReducers: {
        //get todos by name

        // Get all todos
        [getProjectPage.pending]: (state, action) => {
            console.log('Fetching todos from backend ....')
            state.isLoading = true
        },
        [getProjectPage.fulfilled]: (state, action) => {
            console.log('Done')
            state.allPageLoad = action.payload
            state.isLoading = false
        },
        [getProjectPage.rejected]: (state, action) => {
            console.log('Failed to get todos!!!')
            state.isLoading = false
        }
    }
})

// Reducer
const ProjectPageReducer = ProjectPageSlice.reducer

// Selector
export const ProjectPageSelector = state => state.ProjectPageReducer

// Action export
export const {
    findPost
} = ProjectPageSlice.actions

// Export reducer
export default ProjectPageReducer