import { configureStore } from '@reduxjs/toolkit'
import todosReducer from './reducers/ServicesReducer'
import ServicesListReducer from './reducers/ServicesListReducer'
import HeroBannersReducer from './reducers/HeroBannersReducers'
import OverviewReducer from './reducers/OverviewReducers'
import FeaturesListReducer from './reducers/FeaturesListReducers'
import HeaderPageReducer from './reducers/HeaderPageReducers'
import ServicesDetailReducer from './reducers/ServicesDetailReducers'
import ServicesDetailListImgReducer from './reducers/ServicesDetailListImgReducers'
import ServicesDetailListReducer from './reducers/ServicesDetailListReducers'
import ProjectReducer from './reducers/ProjectReducers'
import ProjectListReducer from './reducers/ProjectListReducers'
import BlogReducer from './reducers/BlogReducers'
import BlogListReducer from './reducers/BlogListReducers'
import ContactInformationReducer from './reducers/ContactInformationReducer'
import ContactInformationListReducer from './reducers/ContactInformationListReducer'
import ContentUsReducer from './reducers/ContentUsReducer'
import MapReducer from './reducers/MapReducer'
import AboutReducer from './reducers/AboutReducers'
import SkillItemReducer from './reducers/SkillItemReducers'
import CounterReducer from './reducers/CounterReducer'
import CounterListReducer from './reducers/CounterListReducer'
import TeamReducer from './reducers/TeamReducers'
import TeamListReducer from './reducers/TeamListReducers'
import TestimonialReducer from './reducers/TestimonialReducers'
import TestimonialListReducer from './reducers/TestimonialListReducers'
import BrandReducer from './reducers/BrandReducers'
import BrandListReducer from './reducers/BrandListReducers'
import UsersReducer from './reducers/UserReducers'
import HomePageReducer from './reducersrenew/HomePageReducers'
import AboutPageReducer from './reducersrenew/AboutReducers'
import ServicesPageReducer from './reducersrenew/ServicesReducers'
import ServiceDetailPageReducer from './reducersrenew/ServiceDetailReducers'
import ProjectPageReducer from './reducersrenew/ProjectPageReducers'
import BlogPageReducer from './reducersrenew/BlogPageReducers'
import ContactPageReducer from './reducersrenew/ContactPageReducers'

// Store
const store = configureStore({
    reducer: {
        todosReducer,
        ServicesListReducer,
        HeroBannersReducer,
        OverviewReducer,
        FeaturesListReducer,
        HeaderPageReducer,
        ServicesDetailReducer,
        ServicesDetailListImgReducer,
        ServicesDetailListReducer,
        ProjectReducer,
        ProjectListReducer,
        BlogReducer,
        BlogListReducer,
        ContactInformationReducer,
        ContactInformationListReducer,
        ContentUsReducer,
        MapReducer,
        AboutReducer,
        SkillItemReducer,
        CounterReducer,
        CounterListReducer,
        TeamReducer,
        TeamListReducer,
        TestimonialReducer,
        TestimonialListReducer,
        BrandReducer,
        BrandListReducer,
        UsersReducer,
        HomePageReducer,
        ServicesPageReducer,
        ServiceDetailPageReducer,
        ProjectPageReducer,
        BlogPageReducer,
        ContactPageReducer,
        AboutPageReducer
    }
});

// Export
export { store }