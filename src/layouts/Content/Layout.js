import React from "react"
import { Routes, Route } from "react-router-dom";

import Header from "../Header/Header"
import Nav from "../Navigation/Nav"
import Footer from "../Footer/Footer"
import Homepage from "../../pages/Home/Home";
import About from "../../pages/About/About";
import Services from "../../pages/Services/Services";
import ServicesDetail from "../../pages/Services/ServicesDetail";
import Project from "../../pages/Projects/Projects";
import Blog from "../../pages/Blog/Blogs";
import Contact from "../../pages/Contact/Contact"
import BackToTop from '../../component/BackToTop';
import LoginForm from '../../component/Login/loginForm';
import Admin from "../../pages/Admin/Admin";
import { useLocation } from 'react-router-dom'

const Layout = () => {
  {

    let body
    let location = useLocation();
    const id_page = location.pathname.replace("/", "").replace("/", "");

    if (id_page === "login")
      body = (
        <>
          <Routes>
            <Route path="/login" element={<LoginForm />}></Route>
          </Routes>
        </>
      )
    else if (id_page === "admin-firebase")
        body =(
          <>
            <Routes>
              <Route path="/admin-firebase" element={<Admin />}></Route>
            </Routes>
          </>
        )
    else
      body = (
        <>
          <Header />
          <Nav />
          <main>
            <Routes>
              <Route path="/" element={<Homepage />}></Route>
              <Route path="/about" element={<About />}></Route>
              <Route path="/services" element={<Services />}></Route>
              <Route path="/services-detail" element={<ServicesDetail />}></Route>
              <Route path="/project" element={<Project />}></Route>
              <Route path="/blog" element={<Blog />}></Route>
              <Route path="/contact" element={<Contact />}></Route>
            </Routes>
          </main>
          <Footer />
          <BackToTop />
        </>
      )


    return (
      <>
        {body}
      </>
    )
  }
}
export default Layout;