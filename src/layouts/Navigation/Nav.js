import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import SiteNav from "../Navigation/SiteNav"
import "../Navigation/Nav.css"

export default function Nav() {
  const [isSticky, setIsSticky] = useState(false)
  const [resize, setResize] = useState(window.innerWidth)
  const [show, setShow] = useState(false)

  useEffect(() => {
      const handleScroll = () => {
          if (window.scrollY >= 200) {
              setIsSticky(true)
          } else {
              setIsSticky(false)
          }
      }

      window.addEventListener('scroll', handleScroll);

      return () => {
          window.removeEventListener('scroll', handleScroll);
      }
  }, [])

  useEffect(() => {
      const handleResize = () =>{
        setResize(window.innerWidth)
    }

    window.addEventListener('resize', handleResize);

    return () => {
        window.removeEventListener('resize', handleResize);
    }
  }, [])
  useEffect(() => {
    if (show !== false) {
      console.log(`The name is now... ${show}!`)
    }
  }, [show])
  const MenuClick = (e) => {
      if (show === true){
        // setShow(false)
        setShow(show => !show)
    }else {
      // setShow(true)
      setShow(show => !show)
    }
  }
  return ( 
    <>
    <div className="preloader preloader-deactivate">
		<div className="loader">
			<div className="shadow"></div>
			<div className="box"></div>
		</div>
	</div>
<div className={`navbar-area  ${isSticky ? 'is-sticky' : ''}`}>
		<div className="techvio-responsive-nav">
			<div className="container">
				<div className="techvio-responsive-menu mean-container">
        {resize && (
            <>
              <div id= "menu-nav" className="mean-bar" >
              <a href="#nav" className="meanmenu-reveal" onClick={MenuClick}>
              {show ? 'X' : <><span></span><span></span><span></span></>}
              </a>
              {show ? (
              <nav className= {`mean-nav  ${show ? 'mean-nav-open' : ''}`}>
                <SiteNav />
              </nav>
              ):('')}
              </div>
            </>
            )}
       
					<div className="logo">
          <Link className="logo-link" to="/">
          <img className="white-logo" src="https://cutesolution.com/html/Techvio/assets/img/logo.png" alt="logo" />
          <img className="black-logo" src="https://cutesolution.com/html/Techvio/assets/img/logo-black.png" alt="logo" />
        </Link>
					</div>
				</div>
			</div>
		</div>
		<div className="techvio-nav">
			<div className="container">
				<nav className="navbar navbar-expand-md navbar-light">
        <Link className="logo-link navbar-brand" to="/">
        <img className="white-logo" src="https://cutesolution.com/html/Techvio/assets/img/logo.png" alt="logo" />
          <img className="black-logo" src="https://cutesolution.com/html/Techvio/assets/img/logo-black.png" alt="logo" />
        </Link>
					<div className="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
					<SiteNav />
						<div className="other-option">
            <Link className="default-btn" to="mailto:demo@example.com">Get It Support<span></span></Link>
						</div>
					</div>
				</nav>
			</div>
		</div>
	</div>
  </>
  );
}

