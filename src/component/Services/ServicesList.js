import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { ServicesPageSelector } from '../../store/reducersrenew/ServicesReducers'
import "../Services/Service.css"

function ServicesList({ParentPage, index}){
  const id_page = 'ServicesList'; 
  let todosSelector
  const ServicesPageSelectors= useSelector(ServicesPageSelector)
  const HomePageSelectors= useSelector(HomePageSelector)
  if (ParentPage === 'services')
  {
    todosSelector = ServicesPageSelectors
  }
  else
  {
    todosSelector = HomePageSelectors
  }
  let todosLoading = todosSelector.allPageLoad
 
  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  

  return (
      <>
    {todosLoading.map(todo => (
       <div key={todo.title} className="col-lg-4 col-md-6">
       <div className="single-services-item">
       <div className="services-icon">
       <img src={todo.image_url} alt={todo.title}/>
       </div>
       <h3>{todo.title}</h3>
       <p>{todo.description}</p>
       <div className="services-btn-link">
           <a href={todo.link} className="services-link">Xem Thêm</a>
       </div>
       </div>
   </div>
    ))}
    </>
  );
}

export default ServicesList;


