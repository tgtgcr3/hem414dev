import React from "react";
import { useSelector } from 'react-redux'
import { ServiceDetailPageSelector } from '../../store/reducersrenew/ServiceDetailReducers'

function ServicesDetailList({ ParrentPage, index }) {

    const id_page = 'ServicesDetailList';
    const ServiceDetailPageSelectors = useSelector(ServiceDetailPageSelector)
    const todosSelector = ServiceDetailPageSelectors
    let todosServicesDetailList = todosSelector.allPageLoad
    todosServicesDetailList = index === '' ? todosServicesDetailList : todosServicesDetailList.filter(todo => todo.index === index && todo.id_page === id_page)

    return (
        <>
            {todosServicesDetailList.map((todo, index) => {
                return (
                    <div key={index} className="col-lg-6 col-md-12">
                        <div className="services-step-content">
                            <p>{todo.ServicesDetailList_description}</p>
                            <div className="features-text">
                                <h4>{todo.ServicesDetailList_title1}</h4>
                                <p>{todo.ServicesDetailList_description_title1}</p>
                            </div>
                            <div className="features-text">
                                <h4>{todo.ServicesDetailList_title2}</h4>
                                <p>{todo.ServicesDetailList_description_title2}</p>
                            </div>
                        </div>
                    </div>
                    
                );
            })}
        </>
    );
}

export default ServicesDetailList;