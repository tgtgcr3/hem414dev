import React from "react";
import ServicesList from "../Services/ServicesList";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { ServicesPageSelector } from '../../store/reducersrenew/ServicesReducers'
import "../Services/Service.css"
import { useLocation } from 'react-router-dom'
 

function Services({ index }) {
  const id_page = 'Services';
  let location = useLocation();
  const ParentPage = location.pathname.replace("/", "").replace("/", "");
  const ServicesPageSelectors= useSelector(ServicesPageSelector)
  const HomePageSelectors= useSelector(HomePageSelector)
  let todosSelector

  if (ParentPage === 'services')
  {
    todosSelector = ServicesPageSelectors
  }
  else
  {
    todosSelector = HomePageSelectors
  }

  let todosLoading = todosSelector.allPageLoad
  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

  return (
    <>

      <section className="services-section section-padding">
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              {todosLoading.map((todo) => {
                return (
                  <div key={todo.services_heading} className="section-title">
                    <h6>{todo.services_heading}</h6>
                    <h2>{todo.services_des}</h2>
                  </div>
                )
              })}
            </div>
          </div>
          <div className="row">
            <ServicesList ParentPage={ParentPage} index="1" />
          </div>
        </div>
      </section>

    </>
  );
}

export default Services;
