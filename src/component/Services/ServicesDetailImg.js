import React from "react";
import { useSelector } from 'react-redux'
import { ServiceDetailPageSelector } from '../../store/reducersrenew/ServiceDetailReducers'
import Slider from "react-slick";


function ServicesDetailListImg({ParrentPage, index}){
    
    const id_page = 'ServicesDetailListImg';
    const ServiceDetailPageSelectors = useSelector(ServiceDetailPageSelector)
    const todosSelector = ServiceDetailPageSelectors
    let todosServicesDetailListImg = todosSelector.allPageLoad
    todosServicesDetailListImg = index === ''?todosServicesDetailListImg:todosServicesDetailListImg.filter(todo => todo.index === index && todo.id_page === id_page)


    const settings = {
        className: 'image-sliders',
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        infinite: true,
        speed: 500,
        autoplay:true,
        responsive: [{
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
      };
  return (
      <>
        <Slider {...settings}>
            {todosServicesDetailListImg.map((todo, index) => {
                return (
                    <div key={index} className="services-details-image">
                        <img src={todo.image_url} alt="image" />
                    </div>
                );
            })}
        </Slider>
    </>
  );
}

export default ServicesDetailListImg;


