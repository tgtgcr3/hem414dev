import React from "react";
import { useSelector } from 'react-redux'
import { ServiceDetailPageSelector } from '../../store/reducersrenew/ServiceDetailReducers'
import ServicesDetailListImg from "../../component/Services/ServicesDetailImg"
import ServicesDetailList from "../../component/Services/ServicesDetailList"
import "../Services/ServicesDetai.css"
function ServicesDetail({ index }) {
    const id_page = 'ServicesDetail';
    const ServiceDetailPageSelectors = useSelector(ServiceDetailPageSelector)
    const todosSelector = ServiceDetailPageSelectors
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

    // Display the result on the page
    return (
        <>
            <div >
                {todosLoading.map((todo, index_key) => (
                    <div key={index_key} >
                        <div className="col-lg-12 col-md-12">
                            <ServicesDetailListImg id_page={id_page} index={index} />
                        </div>
                        <div className="col-lg-12 col-md-12">
                            <div className="services-step-wrapper">
                                <div className="services-step-title">
                                    <h2>{todo.ServicesDetail_title}</h2>
                                </div>
                                <div className="row">
                                    <ServicesDetailList id_page={id_page} index={index} />
                                </div>
                            </div>
                        </div>
                    </div>
                ))
                }
            </div>
        </>

    );
}

export default ServicesDetail;
