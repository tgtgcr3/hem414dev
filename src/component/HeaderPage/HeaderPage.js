import React from "react";
import { useSelector } from 'react-redux'
import { ServicesPageSelector } from '../../store/reducersrenew/ServicesReducers'
import { ServiceDetailPageSelector} from '../../store/reducersrenew/ServiceDetailReducers'
import { ProjectPageSelector } from '../../store/reducersrenew/ProjectPageReducers'
import { BlogPageSelector } from '../../store/reducersrenew/BlogPageReducers'
import { ContactPageSelector } from '../../store/reducersrenew/ContactPageReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import "../HeaderPage/HeaderPage.css";
import { useLocation } from 'react-router-dom'

function HeaderPage({ index }) {
	const id_page = 'HeaderPage';
	let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");

	const ServicesPageSelectors = useSelector(ServicesPageSelector)
	const ServiceDetailPageSelectors = useSelector(ServiceDetailPageSelector)
	const ProjectPageSelectors = useSelector(ProjectPageSelector)
	const BlogPageSelectors = useSelector(BlogPageSelector)
	const ContactPageSelectors = useSelector(ContactPageSelector)
	const AboutPageSelectors = useSelector(AboutPageSelector)
	let todosSelector
	if (ParentPage === 'services') {
		todosSelector = ServicesPageSelectors
	}
	else if(ParentPage === 'project')
	{
		todosSelector = ProjectPageSelectors
	}
	else if(ParentPage === 'blog')
	{
		todosSelector = BlogPageSelectors
	}
	else if(ParentPage === 'contact')
	{
		todosSelector = ContactPageSelectors
	}
	else if(ParentPage === 'about')
	{
		todosSelector = AboutPageSelectors
	}
	else 
	{
		todosSelector = ServiceDetailPageSelectors
	}
	let todosLoading = todosSelector.allPageLoad
	todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

	return (
		<>
			{
				todosLoading.map((todo) => (
					<div key={todo.HeaderPage_title} className="page-title-area" style={{ backgroundImage: `url(${todo.HeaderPage_background})` }}>
						<div className="d-table">
							<div className="d-table-cell">
								<div className="container">
									<div className="page-title-content">
										<h2>{todo.title}</h2>
										<ul>
											<li><a href="/">Home</a></li>
											<li>{todo.HeaderPage_title}</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				))
			}
		</>

	);
}
export default HeaderPage

