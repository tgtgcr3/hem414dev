import React from "react";
import CounterList from "../Counter/CounterList"
import "../Counter/Counter.css"

function Counter({index}){
  const id_page = 'Counter'; 

  return (
    <section className="counter-area section-padding">
		<div className="container">
			<div className="row">
                <CounterList id_page={id_page} index="1"/>
			</div>
		</div>
	</section>
  );
}

export default Counter;
