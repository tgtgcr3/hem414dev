import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'

function CounterList({ParentPage, index}){
    const id_page = 'CounterList'; 
    const todosSelector = useSelector(HomePageSelector)
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
    <>
    {todosLoading.map((todo, index) => {
        return (
            <div key={index} className="col-lg-3 col-md-6 counter-item">
                <div  className="single-counter">
                    <div className="counter-contents">
                        <h2>
                            <span className="counter-number">{todo.number}</span>
                            <span>+</span>
                        </h2>
                        <h3 className="counter-heading">{todo.heading}</h3>
                    </div>
                </div>
            </div>
        );
    })}
   </>
  );
}

export default CounterList;



   