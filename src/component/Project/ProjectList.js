import React from "react";
import { useSelector } from 'react-redux'
import { ProjectPageSelector } from '../../store/reducersrenew/ProjectPageReducers'
import "../Services/Service.css"

function ProjectList({ParrentPage, index}){
  const id_page = 'ProjectList';

	const ProjectPageSelectors = useSelector(ProjectPageSelector)
	let todosSelector = ProjectPageSelectors
	let todosLoading = todosSelector.allPageLoad
    todosLoading = index === ''?todosLoading:todosLoading.filter(todo => todo.index === index && todo.id_page === id_page && todo.data.includes(todosSelector.project_name))

   
  return (
      <>
       
    {todosLoading.map((todo, index) => (
       <div key={index} className={`col-6 col-md-4 project-grid-item ${todo.data}`}>
       <div className="project-item">
           <img src={todo.image_url} alt="image" />
           <div className="project-content-overlay">
               <p>{todo.title}</p>
               <h3><a href="single-projects.html">{todo.description}</a></h3>
           </div>
       </div>
        </div>
    ))}
    </>
  );
}

export default ProjectList;


