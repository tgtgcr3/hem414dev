import React, {useEffect} from "react";
import { useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { ProjectPageSelector,findPost } from '../../store/reducersrenew/ProjectPageReducers'

import ProjectList from "../Project/ProjectList"
import Masonry from 'react-masonry-css'
import "../../component/Project/Project.css"


function Project ({index}) {
    const id_page = 'Project';

	const ProjectPageSelectors = useSelector(ProjectPageSelector)
	let todosSelector = ProjectPageSelectors
	const project_name = todosSelector.project_name
	let todosLoading = todosSelector.allPageLoad
    todosLoading = index === ''?todosLoading:todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

    const dispatch = useDispatch()
    const choosePost = todotab => {
        dispatch(findPost(todotab))
    }
    
  return (
    <>
    <section className="project-area section-padding">
        <div className="container">
			<div className="row">
				<div className="col-lg-12 col-md-12">
					<div className="section-title">
						<h6>Recent Works</h6>
						<h2>Our Portfolio</h2>
					</div>
				</div>
			</div>
            <div className="row">
                <div className="col-md-12">
                    <div className="project-list">
                       <ul className="nav">
                            {todosLoading.map((todo, index) => {
                                return(
                                    <li id={todo.tab} onClick={choosePost.bind(this, todo.tab)} key={index} className={project_name === todo.tab ? 'filter filter-active' : 'filter'} data-group={todo.tab}>{todo.tab}</li>
                                )
                            })}
                       </ul>
                    </div>
                    <Masonry  breakpointCols={1}
                    className="my-masonry-grid"
                    columnClassName="my-masonry-grid_column"> 
                    <div className="project-container">
                    <ProjectList id_page={id_page} index="1"/>
                    </div>
                </Masonry>
            </div>
        </div>
        </div>
    </section>
    </>
  );
}

export default Project;
