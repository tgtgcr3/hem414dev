import React, {useEffect} from "react";
import { useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { ContactPageSelector } from '../../store/reducersrenew/ContactPageReducers'
import ContactInformationList from "../Contact/ContactInformationList"
import "../Contact/ContactInformation.css"

function ContactInformation({index}){
    const id_page = 'ContactInformation';
  const ContactPageSelectors= useSelector(ContactPageSelector)
  
  let todosSelector = ContactPageSelectors
  let todosLoading = todosSelector.allPageLoad
  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

  
  return (
	  <>
    <section className="contact-info-wrapper">
        <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <div className="section-title">
                       {todosLoading.map((todo, index) => {
                    return (
                        <div key={index}>
                        <h6>{todo.title}</h6>
                        <h2>{todo.subtitle}</h2>
                        </div>
                    )
                })}
                    </div>
                </div>
                 <ContactInformationList id_page={id_page} index="1"/>
                
            </div>
        </div>
    </section>

	</>
  );
}

export default ContactInformation;