import React, {useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux'
import { ContactPageSelector } from '../../store/reducersrenew/ContactPageReducers'

function ContactInformationList({ParrentPage, index}){
  const id_page = 'ContactInformationList';
  const ContactPageSelectors= useSelector(ContactPageSelector)
  
  let todosSelector = ContactPageSelectors
  let todosLoading = todosSelector.allPageLoad
  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
    <>
    {todosLoading.map((todo, index) => {
        return (
            <div key={index} className="col-lg-4 col-md-6">
                <div className="contact-info-content">
						<h5>{todo.title}</h5>
						<p>{todo.subtitle}</p>
						<a href="tel:${todo.phone}" >{todo.phone}</a>
						<a href="mailto:${todo.email}">{todo.email}</a>
					</div>
            </div>
        );
    })}
    </>
  );
}

export default ContactInformationList;


