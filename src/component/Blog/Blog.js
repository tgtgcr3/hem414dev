import React from "react";
import { useSelector } from 'react-redux'
import { BlogPageSelector } from '../../store/reducersrenew/BlogPageReducers'
import BlogList from "../Blog/BlogList"
import "../Blog/Blog.css"

function Blog({index}){

	const id_page = 'Blog'; 
    const todosSelector = useSelector(BlogPageSelector)
    let todosLoading = todosSelector.allPageLoad

    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
	  <>
	
    <section className="blog-section bg-grey pt-100 pb-70">
		<div className="container">
			<div className="section-title">
				{todosLoading.map((todo, index) => {
					return (
						<div key={index}>
						<h6>{todo.title}</h6>
						<h2>{todo.subtitle}</h2>
						</div>
					)
				})}
			</div>
			<div className="row">
				<BlogList id_page={id_page} index="1"/>
			</div>
		</div>
	</section>

	</>
  );
}

export default Blog;