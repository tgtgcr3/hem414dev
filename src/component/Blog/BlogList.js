import React, {useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux'
import { BlogPageSelector } from '../../store/reducersrenew/BlogPageReducers'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserAlt, faCalendarWeek } from '@fortawesome/free-solid-svg-icons';

function BlogList({ParrentPage, index}){

	const id_page = 'BlogList'; 
    const todosSelector = useSelector(BlogPageSelector)
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
    <>
    {todosLoading.map((todo, index) => {
        return (
            <div key={index} className="col-lg-4 col-md-6">
                <div className="blog-item">
                    <div className="blog-image">
                        <a href="single-blog.html">
                            <img src={todo.image_url} alt={todo.name}/>
                        </a>
                    </div>
                    <div className="single-blog-item">
                        <ul className="blog-list">
                            <li>
                                <a href="#"> <FontAwesomeIcon icon={faUserAlt} /> Author</a>
                            </li>
                            <li>
                                <a href="#"> <FontAwesomeIcon icon={faCalendarWeek} /> 17 June 2021</a>
                            </li>
                        </ul>
                        <div className="blog-content">
                            <h3>
                                <a href="single-blog.html">
                                    {todo.name}
                                </a>
                            </h3>
                            <p>{todo.content}</p>
                            <div className="blog-btn"> <a href={todo.link} className="blog-btn-one">{todo.button}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    })}
    </>
  );
}

export default BlogList;


