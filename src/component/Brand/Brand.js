import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import BrandList from "../Brand/BrandList"
import "../Brand/Brand.css"
import { useLocation } from 'react-router-dom'


function Brand({index}){
    const id_page = 'Brand'; 
	let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");
	let todosSelector 
	const HomePageSelectors= useSelector(HomePageSelector)
	const AboutPageSelectors = useSelector(AboutPageSelector)
	if(ParentPage === 'about')
	{
		todosSelector = AboutPageSelectors
	}
	else 
	{
		todosSelector = HomePageSelectors
	}
    
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
    <section className="partner-section pt-100 pb-70">
		<div className="container">
		{todosLoading.map((todo, index) => {
        return (
			<div key={index}className="partner-title">
				<h6>{todo.title}</h6>
				<h2>{todo.subtitle}</h2>
			</div>
		)})}
			<div className="partner-list">
				<BrandList ParentPage={ParentPage} index="1"/>
			</div>
		</div>
	</section>
  );
}

export default Brand;