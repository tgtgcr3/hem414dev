import React, {useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
function BrandList({ParentPage, index}){
    const id_page = 'BrandList'; 

    let todosSelector 
	const HomePageSelectors= useSelector(HomePageSelector)
	const AboutPageSelectors = useSelector(AboutPageSelector)
	if(ParentPage === 'about')
	{
		todosSelector = AboutPageSelectors
	}
	else 
	{
		todosSelector = HomePageSelectors
	}

    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

  return (
      <>
        {todosLoading.map((todo, index) => {
            return (
                <div key={index} className="partner-item">
                    <a href={todo.link}>
                        <img src={todo.image_url} alt={todo.image_url}/>
                    </a>
                </div>
            );
        })}
    </>
  );
}

export default BrandList;
