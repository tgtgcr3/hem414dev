import React, {useEffect} from "react";
import { useLocation } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import {AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import SkillItem from "./SkillItem"
import "../About/About.css"

function About({index}){
    const id_page = 'About';
	let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");

	const AboutPageSelectors = useSelector(AboutPageSelector)
	let todosSelector = AboutPageSelectors
	
	let todosLoading = todosSelector.allPageLoad
	todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
      <>
    {todosLoading.map((todo, index) => {
        return (
    <section key={index} className="about-area bg-grey section-padding">
    <div className="container">
        <div className="row d-flex align-items-center">
            <div className="col-lg-6 col-md-12 col-sm-12">
            <div className="about-content">
                <div className="about-content-text">
                
                            <h6>{todo.subtitle}</h6> 
                            <h2>{todo.title}</h2>
                            <p>{todo.description}</p>
                        
                    <div className="skills">
                        <SkillItem id_page={id_page} index="1"/>
                    </div>
                    <div className="about-btn-box"> 
                        <a className="default-btn project-btn-1" href={todo.link}>{todo.button}<span ></span></a>
                    </div>
                </div>
            </div>
            </div>
            <div className="col-lg-5 offset-lg-1 col-md-12 col-sm-12">
                <div className="about-image">
                    <img src={todo.image_url} alt="About image" />
                    <div className="years-design">
                        <h2>15</h2>
                        <h5>Năm Kinh Nghiệm</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    )})}
</>
  );
}

export default About;

