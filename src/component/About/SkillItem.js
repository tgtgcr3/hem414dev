import React, {useEffect} from "react";
import { useSelector, useDispatch } from 'react-redux'
import {AboutPageSelector} from '../../store/reducersrenew/AboutReducers'

function SkillItem({ParentPage, index}){
  const id_page = 'SkillItem';
  const AboutPageSelectors= useSelector(AboutPageSelector)
  let todosSelector = AboutPageSelectors

  let todosLoading = todosSelector.allPageLoad

  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
    <div>
    {todosLoading.map((todo, index) => {
        return (
            <div key={index} className="skill-item">
            <h6>{todo.title} <em>{todo.progres}</em></h6>
            <div className="skill-progress">
            <div className="progres" data-value={todo.progres} style={{width:`${todo.progres}`}}></div>
            </div>
            </div>
        );
    })}
    </div>
  );
}

export default SkillItem;

   