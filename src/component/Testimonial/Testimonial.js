import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import TestimonialList from "./TestimonialList"
import "./Testimonial.css"
import { useLocation } from 'react-router-dom'

function Testimonial({index}){
    const id_page = 'Testimonial'; 
    let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");

    let todosSelector
    const HomePageSelectors = useSelector(HomePageSelector)
    const AboutPageSelectors = useSelector(AboutPageSelector)
    if(ParentPage === 'about')
	{
		todosSelector = AboutPageSelectors
	}
	else 
	{
		todosSelector = HomePageSelectors
	}

    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

  return (
    <section className="testimonial-section pt-100 pb-50">
    <div className="container">
    {todosLoading.map((todo, index) => {
        return (
        <div key={index} className="section-title">
            <h6>{todo.title}</h6>
            <h2>{todo.subtitle}</h2>
        </div>
        )})}
        <div className="row">
            <div className="col-lg-12 col-md-12">
                <div className="testimonial-slider owl-carousel owl-theme owl-loaded owl-drag">   
                    <TestimonialList ParentPage={ParentPage} index="1"/>
                </div>
            </div>
        </div>
    </div>
</section>
  );
}

export default Testimonial;

