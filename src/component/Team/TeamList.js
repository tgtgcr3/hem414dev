import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter,faFacebookF, faLinkedin } from '@fortawesome/free-brands-svg-icons'

function TeamList({ParentPage, index}){ 
	const id_page = 'TeamList'; 
    let todosSelector 
    const HomePageSelectors = useSelector(HomePageSelector)
    const AboutPageSelectors = useSelector(AboutPageSelector)
    if(ParentPage === 'about')
	{
		todosSelector = AboutPageSelectors
	}
	else 
	{
		todosSelector = HomePageSelectors
	}
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

  return (
   <>
    {todosLoading.map((todo, index) => {
        return (
            <div key={index}  className="col-lg-3 col-md-6 team-item">
            <div className="single-team-box">
                <div className="team-image">
                    <img src={todo.image_url} alt={todo.name}/>
                    <div className="team-social-icon">
                        <a href="#" className="social-color-1"><FontAwesomeIcon icon={faFacebookF} /></a>
                        <a href="#" className="social-color-2"><FontAwesomeIcon icon={faTwitter} /></a>
                        <a href="#" className="social-color-3"><FontAwesomeIcon icon={faLinkedin} /></a>
                    </div>
                </div>
                <div className="team-info">
                <h3>{todo.name}</h3>
                <span>{todo.job}</span>
            </div>
        </div>
        </div>
        );
    })}
    </>
  );
}

export default TeamList;
