import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import TeamList from "../Team/TeamList"
import "../Team/Team.css"
import { useLocation } from 'react-router-dom'

function Team({index}){
	const id_page = 'Team'; 
	let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");

    let todosSelector 
	const HomePageSelectors= useSelector(HomePageSelector)
	const AboutPageSelectors = useSelector(AboutPageSelector)
	if(ParentPage === 'about')
	{
		todosSelector = AboutPageSelectors
	}
	else 
	{
		todosSelector = HomePageSelectors
	}

    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)

  return (
    <section className="team-area section-padding">
		<div className="container">
			<div className="row">
				<div className="col-md-12">
				{todosLoading.map((todo, index) => {
        		return (
					<div key={index}className="section-title">
						<h6>{todo.title}</h6>
						<h2>{todo.subtitle}</h2>
					</div>
				)})}
				</div>
                <TeamList ParentPage={ParentPage} index="1"/>
			</div>
		</div>
	</section>
  );
}

export default Team;