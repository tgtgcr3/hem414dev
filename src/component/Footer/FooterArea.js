import React from "react";
import FooterLink from "./FooterLink"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTwitter,faFacebookF, faLinkedin, faPinterest } from '@fortawesome/free-brands-svg-icons'
import { faEnvelope, faPhoneVolume, faLocationDot } from '@fortawesome/free-solid-svg-icons';

function FooterArea(props){
  return (
    <div className="footer-area ptb-100">
            <div className="container">
                <div className="row">
                    <div className="col-lg-4 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <div className="footer-heading">
                                <h3>{props.footer_heading1}</h3>
                            </div>
                            <p>{props.footer_content}</p>
                            <ul className="footer-social">
                                <li>
                                    <a href="#"> <FontAwesomeIcon icon={faFacebookF} />
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><FontAwesomeIcon icon={faTwitter} />
                                    </a>
                                </li>
                                <li>
                                    <a href="#"> <FontAwesomeIcon icon={faPinterest} />
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><FontAwesomeIcon icon={faLinkedin} />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-2 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <div className="footer-heading">
                                <h3>{props.footer_heading2}</h3>
                            </div>
                            <ul className="footer-quick-links">
                                <FooterLink props={props} FooterLink={props.FooterLink}/>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-2 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <div className="footer-heading">
                                <h3>{props.footer_heading3}</h3>
                            </div>
                            <ul className="footer-quick-links">
                                <FooterLink props={props} FooterLink={props.FooterLink2}/>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-4 col-md-6 col-sm-6">
                        <div className="single-footer-widget">
                            <div className="footer-heading">
                                <h3>{props.footer_heading4}</h3>
                            </div>
                            <div className="footer-info-contact"><FontAwesomeIcon icon={faPhoneVolume} /> 
                                <h3>{props.footer_textphone}</h3>
                                <span><a href="tel:`${props.footer_phone}`">{props.footer_phone}</a></span>
                            </div>
                            <div className="footer-info-contact"> <FontAwesomeIcon icon={faEnvelope} />
                                <h3>{props.footer_textmail}</h3>
                                <span><a href="mailto:`${props.footer_mail}`">{props.footer_mail}</a></span>
                            </div>
                            <div className="footer-info-contact"><FontAwesomeIcon icon={faLocationDot} />
                                <h3>{props.footer_textaddress}</h3>
                                <span>{props.footer_address}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
  );
}

export default FooterArea;



