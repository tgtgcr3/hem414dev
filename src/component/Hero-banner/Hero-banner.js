import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { ServicesPageSelector } from '../../store/reducersrenew/ServicesReducers'
import { AboutPageSelector} from '../../store/reducersrenew/AboutReducers'
import "../Hero-banner/Hero-banner.css"
import { useLocation } from 'react-router-dom'

function HeroBanners({ index }) { 
    let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");
    const id_page = 'HeroBanners';
    const ServicesPageSelectors= useSelector(ServicesPageSelector)
    const HomePageSelectors= useSelector(HomePageSelector)
    const AboutPageSelectors= useSelector(AboutPageSelector)

    let todosSelector 
    if (ParentPage === 'services')
    {
      todosSelector = ServicesPageSelectors
    }
    else if (ParentPage === 'about')
    {
      todosSelector = AboutPageSelectors
    }
    {
      todosSelector = HomePageSelectors
    }
    
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
    let body


    body = (
        <>
            {todosLoading.map(todo => (
                <div key={index} className={`home-area ${todo.HeroBanners_textalgin}`} style={todo.HeroBanners_textalgin == 'style_2' ? { backgroundImage: `url(${todo.HeroBanners_background})` } : {}}>
                    <div className="d-table">
                        <div className="d-table-cell">
                            <div className="container">
                                <div className="row align-items-center">
                                    <div className="col-lg-6 col-md-12">
                                        <div className="main-banner-content">
                                            <h6>{todo.HeroBanners_subtitle}</h6>
                                            <h1>{todo.HeroBanners_title}</h1>
                                            <p>{todo.HeroBanners_content}</p>
                                            <div className="banner-btn">
                                                <a href={todo.HeroBanners_linkbutton1} className="default-btn-one">
                                                    {todo.HeroBanners_button1}
                                                    <span></span>
                                                </a>
                                                <a className="default-btn" href={todo.HeroBanners_linkbutton2}>
                                                    {todo.HeroBanners_button2}
                                                    <span></span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    {todo.HeroBanners_textalgin == 'style_1' ? (
                                        <div className="col-lg-6 col-md-12">
                                            <div className="banner-image">
                                                <img src={todo.HeroBanners_img1} alt="image" />
                                            </div>
                                        </div>) : ('')}

                                </div>
                            </div>
                        </div>
                    </div>
                    {todo.HeroBanners_textalgin == 'style_1' ? (
                        <div className="creative-shape">
                            <img src={todo.HeroBanners_img2} alt="svg shape" />
                        </div>) : ('')}
                </div>
            ))}
        </>
    )


    return (
        <>
            {body}
        </>
    )
}
export default HeroBanners

