import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { ServicesPageSelector } from '../../store/reducersrenew/ServicesReducers'

function FeaturesList({ ParentPage, index }) {
  const id_page = 'FeaturesList';
  const ServicesPageSelectors= useSelector(ServicesPageSelector)
  const HomePageSelectors= useSelector(HomePageSelector)
  let todosSelector 
  if (ParentPage === 'services') {
    todosSelector = ServicesPageSelectors
  }
  else 
  {
    todosSelector = HomePageSelectors
  }

  let todosLoading = todosSelector.allPageLoad

  todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
  return (
    <ul className="features-list">
      {todosLoading.map((todo, index) => {
        return (
          <li key={index}> <span>{todo.title}</span></li>
        );
      })}
    </ul>
  );
}

export default FeaturesList;

