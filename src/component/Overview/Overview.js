import React from "react";
import { useSelector } from 'react-redux'
import { HomePageSelector } from '../../store/reducersrenew/HomePageReducers'
import { ServicesPageSelector } from '../../store/reducersrenew/ServicesReducers'
import FeaturesList from "./FeaturesList"
import "../Overview/Overview.css" 
import { useLocation } from 'react-router-dom'

function Overview({ index }) {
    let location = useLocation();
    const ParentPage = location.pathname.replace("/", "").replace("/", "");
    let todosSelector
    const ServicesPageSelectors= useSelector(ServicesPageSelector)
    const HomePageSelectors= useSelector(HomePageSelector)
    const id_page = 'Overview'; 
    if (ParentPage === 'services')
    {
      todosSelector = ServicesPageSelectors
    }
    else
    {
      todosSelector = HomePageSelectors
    }
    
    let todosLoading = todosSelector.allPageLoad
    todosLoading = index === '' ? todosLoading : todosLoading.filter(todo => todo.index === index && todo.id_page === id_page)
    let body
    
        body = (
            <>
            {todosLoading.map(todo => (
                <section key={todo.index} className={`overview-section section-padding ${todo.overview_left}`}>
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-6">
                                <div className="overview-image">
                                    <img src={todo.overview_img} alt={todo.overview_title} />
                                </div>
                            </div>
                            <div className="col-lg-6">
                                <div className="overview-content">
                                    <h6>{todo.overview_title}</h6>
                                    <h2>{todo.overview_subtitle}</h2>
                                    <p>{todo.overview_description}</p>
                                    <FeaturesList ParentPage={ParentPage} index={index} />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            ))}
            </>
        )
    
    return (
        <>
            {body}
        </>
    );
}

export default Overview;