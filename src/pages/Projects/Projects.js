import React, {useEffect} from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import Project from "../../component/Project/Project";
import { ProjectPageSelector, getProjectPage } from '../../store/reducersrenew/ProjectPageReducers'
import { useSelector, useDispatch } from 'react-redux'
import Spinner from 'react-bootstrap/Spinner'

function ProjectPage() {
    const dispatch = useDispatch()
    const ProjectPageSelectors = useSelector(ProjectPageSelector)

    useEffect(() => {
        dispatch(getProjectPage())
    }, [dispatch])

    let body
    if (ProjectPageSelectors.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {
        body = (
            <>
                <HeaderPage
                    index="1"
                />
                <Project
                    index="1"
                />
            </>)
    }
    return (
        <>
            {body}
        </>
    )
}

export default ProjectPage;