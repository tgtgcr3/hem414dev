import React, { useEffect } from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import ContactInformation from "../../component/Contact/ContactInformation"
import ContactUs from "../../component/Contact/ContactUs"
import Map from "../../component/Contact/Map"
import { ContactPageSelector, getContactPage } from '../../store/reducersrenew/ContactPageReducers'
import { useSelector, useDispatch } from 'react-redux'
import Spinner from 'react-bootstrap/Spinner'


function ContactPage() {
    const dispatch = useDispatch()
    const ContactPageSelectors = useSelector(ContactPageSelector)

    useEffect(() => {
        dispatch(getContactPage())
    }, [dispatch])

    let body
    if (ContactPageSelectors.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {
        body = (
            <><HeaderPage
                index="1"
            />
                <ContactInformation
                    index="1"
                />
                <ContactUs
                    index="1"
                />
                <Map
                    index="1"
                />
            </>
        )
    }
    return (
        <>
            {body}
        </>
    )
}

export default ContactPage;