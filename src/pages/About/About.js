import React, { useEffect } from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import About from "../../component/About/About";
import Counter from "../../component/Counter/Counter";
import Team from "../../component/Team/Team";
import Testimonial from "../../component/Testimonial/Testimonial";
import HeroBanners from "../../component/Hero-banner/Hero-banner";
import Brand from "../../component/Brand/Brand";
import { useSelector, useDispatch } from 'react-redux'
import {
    AboutPageSelector,
    getAboutPage,
} from '../../store/reducersrenew/AboutReducers'
import Spinner from 'react-bootstrap/Spinner'

function AboutPage() {

    const dispatch = useDispatch()
    const AboutPageSelectorSelector = useSelector(AboutPageSelector)


    useEffect(() => {
        dispatch(getAboutPage())
    }, [dispatch])

    let body
    if (AboutPageSelectorSelector.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {
        body = (
            <>
                <HeaderPage
                    index="1" />
                <About
                    index="1" />
                <Counter
                    index="1" />
                <Team
                    index="1" />
                <Testimonial
                    index="1" />
                <HeroBanners
                    index="2"
                />
                <Brand
                    index="1"
                />
            </>
        )
    }

    return (
        <>
            {body}
        </>
    )
}

export default AboutPage;