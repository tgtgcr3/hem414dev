import React, { useEffect }  from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import ServicesDetail from "../../component/Services/ServicesDetai";
import { useSelector, useDispatch } from 'react-redux'
import { ServiceDetailPageSelector, getServiceDetailPage} from '../../store/reducersrenew/ServiceDetailReducers'
import Spinner from 'react-bootstrap/Spinner'

function ServicesDetailPage() {
    const dispatch = useDispatch()
    const ServiceDetailPageSelectors = useSelector(ServiceDetailPageSelector)

    useEffect(() => {
        dispatch(getServiceDetailPage())
    }, [dispatch])

    let body
    if (ServiceDetailPageSelectors.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {
        body = (
            <>
                <HeaderPage
                    index="1"
                />
                <section className="services-details-area ptb-100">
                    <div className="container">
                        <div className="services-details-overview">
                            <div className="row align-items-center">
                                <ServicesDetail
                                    index="1"
                                />
                                <ServicesDetail
                                    index="2"
                                />
                            </div>
                        </div>
                    </div>
                </section>
            </>
        )
    }
    return (
        <>
        {body}
        </>
    )
}

export default ServicesDetailPage;