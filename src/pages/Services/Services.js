import React, { useEffect } from "react";
import HeaderPage from "../../component/HeaderPage/HeaderPage";
import Services from "../../component/Services/Service";
import Overview from "../../component/Overview/Overview";
import HeroBanners from "../../component/Hero-banner/Hero-banner";
import { useSelector, useDispatch } from 'react-redux'
import {
    ServicesPageSelector,
    getServicesPage,
} from '../../store/reducersrenew/ServicesReducers'
import Spinner from 'react-bootstrap/Spinner'

function ServicesPage() {
    const dispatch = useDispatch()
    const ServicesPageSelectors = useSelector(ServicesPageSelector)

    useEffect(() => {
        dispatch(getServicesPage())
    }, [dispatch])

    let body
    if (ServicesPageSelectors.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {
        body = (
            <>
                <HeaderPage
                    index="1"
                />
                <Services
                    index="1"
                />
                <HeroBanners
                    index="1"
                />
                <Overview
                    index="1"
                />
                <Overview
                    index="2"
                />
            </>
        )
    }
    return (
        <>
            {body}
        </>
    )
}

export default ServicesPage;