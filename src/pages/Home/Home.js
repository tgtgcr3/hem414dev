import React, { useEffect } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import { useSelector, useDispatch } from 'react-redux'
import {
    HomePageSelector,
    getHomePage,
} from '../../store/reducersrenew/HomePageReducers'
import HeroBanners from "../../component/Hero-banner/Hero-banner"
import Services from "../../component/Services/Service"
import About from "../../component/About/About"
import Overview from "../../component/Overview/Overview"
import Counter from "../../component/Counter/Counter"
import Testimonial from "../../component/Testimonial/Testimonial"
import Team from "../../component/Team/Team"
import Blog from "../../component/Blog/Blog"
import Brand from "../../component/Brand/Brand"
import "../Home/Home.css"
import Spinner from 'react-bootstrap/Spinner'

function Homepage() {

    const dispatch = useDispatch()
    const HomePageSelectors = useSelector(HomePageSelector)

    useEffect(() => {
        dispatch(getHomePage())
    }, [dispatch])

    let body


    if (HomePageSelectors.isLoading) {
        body = (
            <div className='spinner-container'>
                <Spinner animation='border' variant='info' />
            </div>
        )
    }
    else {

        body = (
            <>
                <HeroBanners
                    index="1"
                />
                <Services
                    index="1"
                />
                <Overview
                    index="1"
                />
                {/* <Overview 
                 index = "2"
                />     */}
                <Counter
                    index="1"
                />
                <Testimonial
                    index="1"
                />
                <Team
                    index="1"
                />
                <HeroBanners
                    index="2"
                />
                <Brand
                    index="1"
                />
            </>

        )
    }
    return (<>
        {body}
    </>
    )
}

export default Homepage;