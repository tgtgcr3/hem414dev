import firebase from 'firebase/compat/app'
import 'firebase/compat/auth'
import 'firebase/compat/firestore'
import 'firebase/compat/storage'
const firebaseConfig = {
    apiKey: "AIzaSyDYO3YkJR454BuCiYm1vXeyeE7UCe10zFU",
    authDomain: "hem414devremake.firebaseapp.com",
    projectId: "hem414devremake",
    storageBucket: "hem414devremake.appspot.com",
    messagingSenderId: "710477255289",
    appId: "1:710477255289:web:fc41bccd564334f245baa8",
    measurementId: "G-ZTQF5FJF0X"
};
if (!firebase.apps.length) firebase.initializeApp(firebaseConfig)
    // const auth = firebase.auth()
const db = firebase.firestore()
    // const storage = firebase.storage()
export default db